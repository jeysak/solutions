INSERT INTO system_user (id,active,creation_timestamp,modification_timestamp, modification_user, password, username )
VALUES (1,1,NOW(),NOW(),"pfort","$2a$10$d1tUB8RQisy02CdRewauRODvx0ue7QU3McM07aoAECvCtpJKyi6uW","pfort");

insert into permission (id,active,creation_timestamp,modification_timestamp, modification_user, description, name)
values (1,1,NOW(),NOW(),"pfort","ROLE_ADMIN","ROLE_ADMIN");

insert into role (id,active,creation_timestamp,modification_timestamp, modification_user, description, name)
values (1,1,NOW(),NOW(),"pfort","Desarrolladores","Deployers");

insert into permission_role_rel (role_id,permission_id )
values (1,1);

insert into user_role_system_user_rel (role_id, system_user_id)
values (1,1);

commit;
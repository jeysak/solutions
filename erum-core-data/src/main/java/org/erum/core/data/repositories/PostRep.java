package org.erum.core.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import org.erum.core.data.entities.Post;

public interface PostRep extends JpaRepository<Post, Long> {

}

package org.erum.core.data.service;

import java.util.List;

import org.erum.core.data.dto.PostDto;
import org.erum.core.data.entities.Post;
import org.erum.core.data.repositories.PostRep;
import org.erum.core.translator.CoreTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PostService {

	@Autowired
	private PostRep postRep;

	public void savePost(PostDto postDto) {
		Post post = CoreTranslator.getInstance().getPost(postDto);
		postRep.save(post);
	}
	public List<Post> getAllPosts() {
		return postRep.findAll();
	}
}

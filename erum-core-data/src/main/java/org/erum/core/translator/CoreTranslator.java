package org.erum.core.translator;

import java.util.ArrayList;
import java.util.List;

import org.erum.core.data.dto.ForumCategoryDto;
import org.erum.core.data.dto.ForumDto;
import org.erum.core.data.dto.PostDto;
import org.erum.core.data.entities.Forum;
import org.erum.core.data.entities.ForumCategory;
import org.erum.core.data.entities.Post;

public class CoreTranslator extends Translator {

	public static CoreTranslator coreTranslator;

	public static CoreTranslator getInstance() {
		if (coreTranslator == null) {
			coreTranslator = new CoreTranslator();
		}
		return coreTranslator;
	}

	// Post
	public PostDto getPostDto(Post post) {
		PostDto postDto = new PostDto();
		fillEqualsToDto(post, postDto);
		return postDto;
	}

	public Post getPost(PostDto postDto) {
		Post post = new Post();
		fillEqualsToPo(postDto, post);
		return post;
	}

	public List<PostDto> getPostDtoList(List<Post> postList) {
		List<PostDto> postDtoList = new ArrayList<PostDto>();
		postList.forEach(post -> postDtoList.add(getPostDto(post)));
		return postDtoList;
	}

	public List<Post> getPostList(List<PostDto> postDtoList) {
		List<Post> postList = new ArrayList<Post>();
		postDtoList.forEach(postDto -> postList.add(getPost(postDto)));
		return postList;
	}

	// Forum
	public ForumDto getForumDto(Forum forum) {
		ForumDto forumDto = new ForumDto();
		fillEqualsToDto(forum, forumDto);
		return forumDto;
	}

	public Forum getForum(ForumDto forumDto) {
		Forum forum = new Forum();
		fillEqualsToPo(forumDto, forum);
		return forum;
	}

	public List<ForumDto> getForumDtoList(List<Forum> forumList) {
		List<ForumDto> forumDtoList = new ArrayList<ForumDto>();
		forumList.forEach(forum -> forumDtoList.add(getForumDto(forum)));
		return forumDtoList;
	}

	public List<Forum> getForumList(List<ForumDto> forumDtoList) {
		List<Forum> forumList = new ArrayList<Forum>();
		forumDtoList.forEach(forumDto -> forumList.add(getForum(forumDto)));
		return forumList;
	}

	// ForumCategory
	public ForumCategoryDto getForumCategoryDto(ForumCategory forumCategory) {
		ForumCategoryDto forumCategoryDto = new ForumCategoryDto();
		fillEqualsToDto(forumCategory, forumCategoryDto);
		return forumCategoryDto;
	}

	public ForumCategory getForumCategory(ForumCategoryDto forumCategoryDto) {
		ForumCategory forumCategory = new ForumCategory();
		fillEqualsToPo(forumCategoryDto, forumCategory);
		return forumCategory;
	}

	public List<ForumCategoryDto> getForumCategoryDtoList(List<ForumCategory> forumCategoryList) {
		List<ForumCategoryDto> forumCategoryDtoList = new ArrayList<ForumCategoryDto>();
		forumCategoryList.forEach(forumCategory -> forumCategoryDtoList.add(getForumCategoryDto(forumCategory)));
		return forumCategoryDtoList;
	}

	public List<ForumCategory> getForumCategoryList(List<ForumCategoryDto> forumCategoryDtoList) {
		List<ForumCategory> forumCategoryList = new ArrayList<ForumCategory>();
		forumCategoryDtoList.forEach(forumCategoryDto -> forumCategoryList.add(getForumCategory(forumCategoryDto)));
		return forumCategoryList;
	}
}

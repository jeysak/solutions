$(function() {
	$("#admin_panel").addClass("active");
	$("table").footable();
});


var myAppModule = angular.module('admin_panel', [ 'ngResource' ]);

myAppModule.factory('PostService', function($resource) {
	return $resource('http://localhost:8080/post');
});

myAppModule.controller('testController', function($scope, $http, PostService) {
	PostService.get({
		name : 'che'
	}, function(data) {
		$scope.post = data.modificationUser;
	});
});

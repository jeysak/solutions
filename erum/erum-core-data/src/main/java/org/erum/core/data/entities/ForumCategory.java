package org.erum.core.data.entities;

import javax.persistence.Entity;

@Entity
public class ForumCategory extends PersistentObject {
	
	private String name;
	
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

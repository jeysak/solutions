package org.erum.core.data.dto;

import java.util.Date;

import org.erum.core.data.entities.Forum;

public class PostDto extends DataTransferObject {

	private String title;

	private Date postDate;

	private Forum forum;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public Forum getForum() {
		return forum;
	}

	public void setForum(Forum forum) {
		this.forum = forum;
	}

}
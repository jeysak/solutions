package org.erum.core.data.repositories;

import org.erum.core.data.entities.Forum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForumRep extends JpaRepository<Forum, Long>{

}

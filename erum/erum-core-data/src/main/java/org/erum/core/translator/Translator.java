package org.erum.core.translator;

import org.erum.core.data.dto.DataTransferObject;
import org.erum.core.data.entities.PersistentObject;
import org.springframework.beans.BeanUtils;

public class Translator {

	protected void fillEqualsToDto (PersistentObject po, DataTransferObject dto) {
		BeanUtils.copyProperties(po, dto);
	}
	
	protected void fillEqualsToPo (DataTransferObject dto, PersistentObject po) {
		BeanUtils.copyProperties(dto, po);
	}
}

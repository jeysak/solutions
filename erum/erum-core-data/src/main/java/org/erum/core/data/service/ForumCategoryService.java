package org.erum.core.data.service;

import org.erum.core.data.dto.ForumCategoryDto;
import org.erum.core.data.entities.ForumCategory;
import org.erum.core.data.repositories.ForumCategoryRep;
import org.erum.core.translator.CoreTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForumCategoryService {

	@Autowired
	private ForumCategoryRep forumCategoryRep;

	public void saveForumCategory(ForumCategoryDto forumCategoryDto) {
		ForumCategory forumCategory = CoreTranslator.getInstance().getForumCategory(forumCategoryDto);
		forumCategoryRep.save(forumCategory);
	}
}

package org.erum.core.data.repositories;

import org.erum.core.data.entities.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemUserRep extends JpaRepository<SystemUser, Long>{

	SystemUser findByUsername(String username);
}

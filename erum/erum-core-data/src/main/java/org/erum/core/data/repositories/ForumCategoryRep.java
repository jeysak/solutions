package org.erum.core.data.repositories;

import org.erum.core.data.entities.ForumCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForumCategoryRep extends JpaRepository<ForumCategory, Long> {

}

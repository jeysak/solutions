package org.erum.core.data.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.erum.core.data.entities.Permission;
import org.erum.core.data.entities.Role;
import org.erum.core.data.entities.SystemUser;
import org.erum.core.data.repositories.SystemUserRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("userSecurityService")
public class UserSecurityService implements UserDetailsService {
	// get user from the database, via Hibernate

	@Autowired
	private SystemUserRep userRep;

	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

		SystemUser user = userRep.findByUsername(username);

		List<GrantedAuthority> authorities = buildUserAuthority(user.getRoles());
		return buildUserForAuthentication(user, authorities);
	}

	// pasa el usuario a usuario de spring
	private User buildUserForAuthentication(SystemUser user, List<GrantedAuthority> authorities) {
		return new User(user.getUsername(), user.getPassword(), user.isActive(), true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Set<Role> groups) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (Role group : groups) {
			for (Permission permission : group.getPermissions()) {
				setAuths.add(new SimpleGrantedAuthority(permission.getName()));				
			}
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}

}

package org.erum.core.data.service;

import org.erum.core.data.dto.ForumDto;
import org.erum.core.data.entities.Forum;
import org.erum.core.data.repositories.ForumRep;
import org.erum.core.translator.CoreTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ForumService {

	@Autowired
	private ForumRep forumRep;
	
	public void saveForum (ForumDto forumDto) {
		Forum forum = CoreTranslator.getInstance().getForum(forumDto);
		forumRep.save(forum);
	}
}

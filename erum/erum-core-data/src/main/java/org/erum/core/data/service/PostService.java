package org.erum.core.data.service;

import org.erum.core.data.dto.PostDto;
import org.erum.core.data.entities.Post;
import org.erum.core.data.repositories.PostRep;
import org.erum.core.translator.CoreTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PostService {

	@Autowired
	private PostRep postRep;
	
	public void savePost(PostDto postDto) {
		Post post = CoreTranslator.getInstance().getPost(postDto);
		postRep.save(post);
	}
}

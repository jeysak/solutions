package org.erum.core.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@SpringBootApplication
@EnableAutoConfiguration
@EntityScan("org.erum.*.data.entities")
@EnableJpaRepositories(basePackages = {"org.erum.*.data.repositories"})
@ComponentScan(basePackages = {"org.erum.*.view","org.erum.*.data.service"})
//@EnableTransactionManagement
@Import({ SecurityConfig.class, ThymeleafConfig.class, MvcConfiguration.class })
public class ApplicationConfig {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationConfig.class, args);
    }
}

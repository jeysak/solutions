package org.erum.core.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

@Controller
public class AdminPanelController {

	@RequestMapping("admin_panel")
	public ModelAndView AdminPanel() {
		return new ModelAndView("admin_panel");
	}
}
